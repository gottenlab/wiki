<!--
Previous week's meeting notes: https://wiki.snowdrift.coop/resources/board-meetings/
Meeting location: https://meet.jit.si/snowdrift
backup: Mumble server: snowdrift.sylphs.net port 8008 (mobile: half-duplex mode avoids echo)
See end of this pad for meeting-practices notes and reference
-->

Snowdrift.coop Board Meeting — November 21, 2020
================================================

<!--

    Personal check-in round

    Assign facilitator

    Assign time keeper, use https://online-timers.com, start for each item, paste each url in the chat

    Assign note taker

    Assign someone to clean and save notes and update pad for next time

-->

1. Reports, Metrics
-----------------------

- Attendees: Aaron, Stephen, Salt, Erik, Athan

- Treasurer's report: $79.18 in NCB checking account, since closing out AWS receieve $83 refund, paid final balance and account is fully cancelled, no ongoing expenses aside from $20 annual report to state of Michigan, need to figure out domain registration and fee for 2021

2. Review previous meeting feedback
---------------------------------------------

- wolftune: I know I talked a lot, more specific feedback would be useful. Want to keep working on my habit of speaking in run on sentences. Feels like we are making progress overall.
- Stephen: earlier break would have been ideal
- Erik: Yes, don't underestimate the value of breaks!! Hope I didn't come off too critical. Good to have big, hairy goals.

3. Approve last meeting's notes
--------------------------------------
https://wiki.snowdrift.coop/resources/board-meetings/2020-08-15-meeting

(Approved unanimously)

4. Review outstanding Board action-steps
----------------------------------------------------
<!-- Mark NEXT STEPs as [DONE] or [CAPTURED] <LINK> by <when?> -->

### OSUOSL Migration
- NEXT STEP (smichel): Send an update on OSUOSL migration status DUE: Aug 20th [DONE]

### Business plan
- <https://gitlab.com/snowdrift/wiki/-/issues/29>
- NEXT STEP (Lisha): share some business-plan templates for us to work from [CAPTURED personally]
- NEXT STEP (Salt): share that Cryptonomicon selection [DONE]
- NEXT STEP (Aaron + Stephen): capture getting a biz-plan in place as a milestone or bulk issue with task list [DONE (link above)]

### Checklist and timeline for OSI graduation
- <https://gitlab.com/snowdrift/governance/-/issues/77>
- NEXT STEP (Aaron): draft a starting checklist for OSI graduation [DONE]
- NEXT STEP (Aaron): draft a starting timeline for OSI graduation 
  - meeting scheduled with Elana at OSI later this month

### Bylaws clearer next steps
- <https://gitlab.com/snowdrift/governance/-/issues/76>
- NEXT STEP (Aaron): One-sentence summary of "membership rights" section, "details we thought of" in a quote block (similar for other sections as appropriate)  [DONE]
- NEXT STEPS (Stephen + Aaron): Cowork on formatting (e.g. drop "Presumed boilerplate"), send to board for async approval  [DONE]
- NEXT STEP (Aaron): Split bylaws-questions-notes.md into checklist of stuff to review when looking at the final draft of the bylaws vs stuff to look at sooner [DONE]

### Carried over
- <https://gitlab.com/snowdrift/governance/-/issues/66>
- NEXT STEP (Aaron): Commitment pledge for Directors [CAPTURED]


5. Agenda topics
--------------------
<!--
One minute silence — check thoughts and notes/tasks/emails to surface tensions
https://www.online-timers.com/timer-1-minute
Add agenda items as needed
Does anyone need to leave early?
Confirm agenda order and timeboxes (in 5 minute increments)
Adjust for anyone who must leave early
Reminder: Last 10 minutes are reserved for standup/eval sections
### TOPIC TEMPLATE (LEADER) <!-- Timebox: # minutes -->
-->

### Bylaws legal review update (Stephen) <!-- Timebox: 5 minutes -->
- Stephen: finished consolidating bullet points into a more concise form and sent to Deb (lawyer). She has another project with a hard deadline of Dec 14, prefers to wait till after that to give us undivided attention. Elana's email sort of implied potential for us to run into next year on OSI incubation
- Aaron: We have around $1000 remaining on old retainer with Deb, didn't recently discuss about costs, need to be clear on questions to make first meeting as effective as possible
- Erik: is there anything we should be addressing on this topic this or next meeting?
- Aaron: Board should see emails with Deb
- Erik: as a homework assignment, we should all read the updated summary points that were sent to her
- Aaron: side-issue: I got an explicit statement of interest from former board member Charles (back on good terms after a pause when he quit the Board in summer 2019). He's working on related issues of co-op coordination online with his own startup. He offered to be involved, and I wanted to get input on what to say to him or how much to do what
- Erik: remind me his background?
- Aaron: Charles does sysadmin work and has experience with managing large projects too. His startup is a mesh network vision with several facets.
- Erik: no opinion from me for now, let's keep him updated
- Erik: we can work asynchronously to review email to deb and discuss
- NEXT STEP (Stephen): share with Board the final bullet points sent to Deb


### 501(c) considerations (Aaron) <!-- Timebox: 5 minutes -->
- Aaron: Wanted Board updated on this topic status. Salt is in grad school in a class with professor who deals with 501(c)3 stuff. Deb does have 501 experience (though not in the realm of open source or online platforms). 501(c)(3) has real advantages, but I (Aaron) suspect also tensions with co-op structure among other things.. If we frame the public wording of our mission that patrons are members who want the *results* of the projects they fund for their own benefit, this goes against (c)(3) mission statement framing
- Stephen: from brief conversation, Deb thought a membership nonprofit might be better fit

- ROUND:

    - smichel: both seem like good things to have: we already have the domain and branding of .coop, no strong feelings
    - Lisha: I think this might be an issue of legal definitions of things vs philosophical/political. have people seen worker-directed non-profits. Might help with thinking about the boundaries between nonprofit and coop. For me (philsophical statement), I think there's something to be said that's beneficial to push through (for both): if we need to tweak wording to fit within the law,  seems reasonable (https://www.theselc.org/worker_selfdirected_nonprofits)
    - Erik: membership based co-op vs nonprofit cooperative: if Deb is pushing us towards membership based, co-op, I'd say go with that. There's a question, is there any activities that might not fit. one question to get clear on :coop vs nonprofit, does one make distribution of funds to a nonprofit eaiser?
    - Aaron: Don't think we have any reason to care about the option of patronage refunds. Our mission has to do with empowering people to funding public goods: there's a co-op aspect of gaining benefit from the public goods (but in no way different than any one else). Note that we can be state-level non-profit, that's independent of 501 somewhat. My concern is: if we apply to IRS, they will wonder about eligibility of co-op status, if anything of ours describes benefits to members, that won't look good for 501(c)(3). If we push to fit 501 and succeed, the worst case scenario would be losing the legal .coop name
- Aaron: since salt is here, any questions for his professor on this tension on co-op vs nonprofit
- https://gitlab.com/snowdrift/governance/-/issues/80
- Salt: an initial motivation was to be multistakeholder coop, and now that we are just looking at only patrons being members, that seems to make it worth really considering 501(c)(3). It was harder to fit that with the multistakeholder idea.
- Aaron: FWIW, the motive for multistakeholder vision was just about including all affected stakeholders in decisions as a democratic value. It wasn't otherwise related to our mission really.
- Aaron: I think updating our mission statement is the key in framing this
- NEXT STEP (Stephen): Schedule board mtg to discuss coop vs nonprofit situation, decide on goals/mission
- NEXT STEP (wolftune): Make sure we are clear on questions for Salt to pass along to his professor.
- NEXT STEP (wolftune, stephen,+ alignwaivers): update mission statement and vision statement

### Officers/roles/scheduling/attendance (Stephen) <!-- Timebox: 5 minutes -->
- smichel: noting that Mickey has missed several meetings (but not to point fingers), I think we could use a formal role of someone like "Cat-herder in chief", someone who preps meetings and schedules and makes sure people get there, reminders and so on
- Erik: sounds good if there's rotating responsibility, you until year end, then me for 6 months
- Erik: doesn't necessarily have to be someone on the board
- Lisha: I think this is a good idea: think the rotating responsibility is fine if it's not every month (and people will forget it's there job). Should be someone who's really good at managing time in the midst of chaos
- Aaron: I wonder if we could have a check-in with Mickey to get her up to speed and figure out how to help her attend next meeting
- smichel: we've been more adhoc with scheduling, want to be more explicit
- Lisha: I think she may be really busy, and more conservative with balancing obligations. I will reach out to her within the next week, I had been meaning to for other reasons anyway
- NEXT STEP (Lisha): connect with Mickey to update / check in about attendance
- NEXT STEP (Stephen): Schedule a meeting, send out reminders in advance
- Salt: do you have officers officially?
- Aaron: I'm the treasurer. I did submit roles when applying to Michigan: I am both treasurer and president, and Stephen is the secretary
- NEXT STEP (Stephen): take scheduling role until end of the year

### MVP crowdmatching decision strategy (Aaron) <!-- Timebox: 10 minutes -->
- Aaron: wanted to get the boards insights and share updates on a major activity in the team currently.
- The live alpha uses a hardcoded budget cap for patrons of $10 a month. The presentation describes that patrons will drop out of pledges if the matching passes their budget limit.
- The proposed alternative mechanism has a per-project goal point that serves as a budget limit. For alpha, that could just be the same cap but everyone keeps donating with matching turned off.
- But now we're having many long disussions about different variations. One of the debates is whether the project goal is in terms of matching patrons or dollars.
- I'm looking for guidance on how to deal with the team making this decision, coordinating between people who want to optimize everything first vs. move forward and iterate an mvp
- Lisha: I think the goal should be to have conversations with the widest audience possible. It's not about being perfect, it's about building a group of people to direct questions to. Also immediately do a publicity push to get people to join in conversations say 20 people at a time, capture opinions and use that to narrow down further. Would say go with the simplest thing to program and have MVP to start such conversations.
- Aaron: I think how we invite people's feedback, it should be structured. I'm trying to negotiate how to get everyone on the team with that plan, or otherwise how to get to a clear plan we all follow for how we move forward.
- Erik: to clarify, budget limit drops the pledges or not?
- Aaron: the current public idea until now was that pledges are dropped if patrons do not increase their budget. The idea is that if we are all agreeing to match each other, it goes against that for some patrons to be maxed out and still count in the list. That would go against the idea of showing that all these patrons in the crowd are offering to match others who join. The original vision was a notification that you've reached your max, do you want to keep contributing or start free-riding (like pass the baton) etc.
- Erik: we have made a public assertion on how the model works, so changes that we make now need to be communicated / consistent as much as possible
- Aaron: We've come to agreement we don't want the negativity of 'kicking out' and we can do better.
- Erik: I think we should keep the launch of the model as is. In terms of getting input, perhaps when onboarding project, we could do it on a per project basis, and use as baseline pilots. I dont think nows the best time to come up with the ultimate model yet.
- Aaron: the budget part of the alpha isn't really active meaningfully yet, so changing that one aspect won't affect current patrons at all. I agree that we can't just change the model for alpha in any more significant way.
- Erik: If gonna be hammered out what the longer-term model should be, consider specific projects such as Krita, figure out parameters and how we could try it
- Lisha: I think the website pages looking consistent and polished for MVP is good, but the exact mechanism's details is less important for launch. Having *A* launch is extremely important, need to move towards as quickly as possible. The experience of signing up should be clean, having one solid option. As Erik said, can reach out and do pilot testing variations: Choose a specific project offering them to define a "pilot" that varies from the MVP we used for Snowdrift.coop (and the MVP is potentially available for other outside projects)
- Steven: I think the biggest obstacle to this right now is lacking volunteers with CSS capacity.
- Lisha: would you like me to reach out to someone?
- NEXT STEP (Lisha): reach out to Mooks in Berlin about CSS help for Snowdrift (he may have free time till january) (Update: I called Moox. He's not able to help with this right now. - Lisha)
- Aaron: Yes, getting to launch point is a big goal. I think improving the framing of the budget limit fits into that launch though. We already know that the kicked-out budget approach gets negative reactions, and the only work to change that is to change our explanations a bit (which was already a task to do).
- Aaron: I separately would like to invite the Board to help facilitate or advise on how the team can have the most effective process in situations where we may be getting stuck or inefficient.
- Erik: I think having larger conversations, maybe an office hour type of discussion, that might be a good way to open up this conversations especially if there is any tension.
- Stephen: Let's block out time at the next board meeting and also potentially team meeting
- NEXT STEP (Stephen): Plan for bringing this up at next Board meeting

Salt added to the notes these reference for people who want to review the current mechanism proposals:

- I suggest people read the first post in this thread from Aaron: https://community.snowdrift.coop/t/re-framing-mvp-alpha-toward-project-goal-direction/1657
- info about mechanisms
    - (old) share-value: https://wiki.snowdrift.coop/archives/communications/formula
    - (current) linear-quadratic: https://wiki.snowdrift.coop/about/mechanism
    - (upcoming, proposed name) goal-scaling: https://community.snowdrift.coop/t/building-consensus-on-crowdmatching-options-for-a-single-goal/1630
    - (upcoming, proposed name) patron-driven: https://community.snowdrift.coop/t/mechanism-proposal-patron-only/1653


### Draft timeline for OSI graduation (carry-over)
- Aaron: don't have a timeline, and could make a deadline but would be arbitrary
- Steven: one date to put on it which is December 15th (when deb can begin working with us)
- NEXT STEP (Aaron): begin a thread on discourse / prompt discussion

5. Open discussion <!-- Timebox: 5 minutes -->
-----------------------


6. Meeting evaluation / feedback / suggestions / appreciations round <!-- Timebox: final 5 minutes -->
--------------------------------------------------------------------------------------
- Erik: the tricky part for keeping meetings on track is realistic timeboxing: to take more time to consciously decide about the time-boxing and the outcomes desired
- Lisha: Checking in on myself repeating the same things forcefully. When we get to a point where our opinions on a matter haven't changed and not moving forward, that's when the topic probably needs to be tabled and conversations move to a different place.  For me right now, conversations are easier than long threads. 
- Aaron: Thanks to Salt for facilitating and Athan for notetaking. I do see a lot of value in taking more time to decide 'what outcome do you want from this topic' - would've been better for me to have taken that time on the last topic: let's make sure 'what you want out of it' is a clear actionable outcome, and not go ahead with a topic until we have that. It's worth the up-front extra time.
- Stephen: quite happy with how most of the meeting went. Had we not had the last topic, we would've finished on time. The point of me wanting to schedule the December was meeting was in case we had too many topics in this meeting but I forgot to bring that up again today. What Aaron wanted out of the last topic wasn't as clear as it could've been.
- Salt: Identifying if it's gonna be an hour or 1.5 at the start would be good: I assume hour if not informed otherwise. I think it was a mistake to remove breakout rooms. There are times where people can stay later, and a topic goes off the wall it makes sense to move it to a breakout room after if people have time. I had some tension around being here as a facilitator, but also being a team member, hard to keep separate.
At 45 minutes there should be a bio break. Thanks everyone for taking time

<!--
Meeting adjourned!
Clean up meeting notes, then add to wiki
Prepare this pad for next meeting:
Clear meetings date, attendee list
Clear report / metrics (keeping the topic titles)
Replace previous meeting eval notes with new and filter it down to essential points
Clear the "last meeting next-steps review" section
Move new NEXT STEPs to that review section and then clear out other notes areas
Change the last-meeting-notes wiki link to this past one
Clear authorship colors
-->

<!--
Meeting best-practices and tools

Etherpad use
- Use chat in etherpad (and add your name)
- Option: audio notifications on firefox via https://addons.mozilla.org/en-US/firefox/addon/notification-sound/

Agenda topics
- Each topic facilitated by topic lead with main facilitator help
- As needed, ping !folks on matrix to read over anything advance, ideally before the day of the meeting

NEXT STEPS
- each topic should capture NEXT STEPS (or clarify that there aren't any)
- should be clear and actionable
- assignee agrees to capture or complete by next week

Timeboxing
- timebox each topic, rounded to nearest 5min., settled during agenda confirmation
- format is: "Timebox: 10 minutes (until hh:mm)" (in an html comment so it doesn't appear in note archives)
- at topic beginning, convert the :mm to expected end time
- at timebox end, "thumb polls" may add 5 minutes at a time
- hand symbols
    - "^" approve, extend the timebox
    - "v" disagree, move onto the next topic
    - "." neutral

Discussion options
- open discussion
- call for a round ("pass the mic" style, facilitator makes sure no one is skipped)
- hand symbols
    - "o/" or "/" means you have something to say and puts you in the queue
    - "c/" or "?" means you have a clarifying question and jumps you to the top of the queue
    - "d" means thumbs up, encouragement, agreement, etc.
    - ">" means you understand someone's point, please move on
    - "d>" indicates feeling complete on an agenda item, ready to move on to

Notetaking
- "???" in notes means something missed, please help capturing what was said
- aim for shorthand / summary / key points (not transcript)
- don't type on the same line as another notetaker ( ok to do helpful edits after but not interfering with current typing)
-->