<!-- Previous week's meeting notes: https://wiki.snowdrift.coop/resources/meetings/2019/ -->
<!-- Meeting location: https://meet.jit.si/snowdrift -->

# Meeting — September 18, 2019

Attendees: Athan, Salt, wolftune, iko, MSiep

<!-- Check-in round -->

## Metrics

Discourse (over past week): <!-- stats from https://community.snowdrift.coop/admin?period=weekly -->

- Signups: 0 -> 1
- New Topics: 1 -> 2
- Posts: 16 -> 16

Sociocracy 3.0, wolftune @ page: 11 of 37

## Reminders on building new best-practice habits

### Sociocracy 3.0 stuff
- Drivers, describing driver statements: https://patterns.sociocracy30.org/describe-organizational-drivers.html
    - Current situation + effect + what's needed + impact of doing it
    - forum discussion of our use: https://community.snowdrift.coop/t/sociocracy-3-0-drivers/1309

### Use of GitLab kanban boards
- Use the boards to pick your tasks, dragging or marking To Do and Doing appropriately
- https://community.snowdrift.coop/t/using-the-kanban-boards-in-gitlab-plus-wolftune-check-in/1263

## Carry-overs

```
## Salt met author of unfinished book on patronage funding at Communications conference
- NEXT STEP (salt): follow up with conference contacts

## design progress, pace / how's h30
- NEXT STEP (wolftune): bug h30 (Henri) about documenting process of implementing mockups <https://gitlab.com/snowdrift/snowdrift/issues/158>
- NEXT STEP (all): recruiting front-end (haskell?) developers

## Issue grooming / dumping etc
- <https://gitlab.com/groups/snowdrift/-/issues?milestone_title=LFNW+2019>
- NEXT STEP (smchel17): Delete or close the LFNW 2019 milestone
- NEXT STEP (smichel + wolftune + alignwaivers): schedule grooming session

```

<!-- New Agenda Down Here  -->

## Mumble
- wolftune: Mumble seems to be working okay, we can do jitsi again if video is really needed on per-case basis
- NEXT STEP (wolftune): update meeting wiki / forum notes
- NEXT STEP (wolftune): add Mumble server to ops notes and backlog for OSU

## New meeting time
- Salt: https://community.snowdrift.coop/t/team-availability-and-weekly-meeting-details/1345
- Salt: I have one more meeting tomorrow or Friday and then I'll know my availability until Dec
- NEXT STEP (alignwaivers): bug everyone to update their availability on whenisgood

## Marking prominently that we're under-construction
- wolftune: we already have a driver statement on the forum, we can refine and use that
- Salt: I think it needs more discussion on the forum
- https://community.snowdrift.coop/t/how-to-make-it-clear-snowdrift-coop-is-not-and-we-realize-its-not-ready-for-prime-time/1361
- NEXT STEP (wolftune): ping people for feedback on driver statement

## SeaGL
- Salt: can we do an announcement that we're coming to SeaGL? even though it's still a bit ahead (some of us have submitted talks)
- Salt: fyi, I'm currently hands full with organising SeaGL stuff and travelling
- wolftune: alignwaivers, how do you feel about doing snowdrift social announcements?
- alignwaivers: I can do that, it ties in with my role
- wolftune: I can set up a banner at the table
- Salt: can someone send an email to SeaGL organisers about being waitlisted for a table? 
- NEXT STEP (alignwaivers): send email to sponsors@seagl.org saying we're okay being waitlisted but happy to have a space

## RMS
- wolftune: I think probably the most productive thing is to focus the topic toward restorative justice and the problems with call-out culture *without* implying that there weren't transgressions
- Salt: I don't think we currently really need to make any official statement
- there are some conversations going around, the most dangerous one being "do we need free software?", which snowdrift could address (if it came up)
- wolftune: there are issues of (A) feminism and inclusion. however, this is different from (B) how do deal with viligante justice and mob calls/outrage culture, which is different from (C) political tactical decisions for orgs and movements
- how someone feels about each has no correlation, any mix of positions can be consistent
- background for people unaware, rms said something related to the epstein scandal, the rms comments were troubling but also misunderstood and exaggerated
- rms eventually resigned from FSF as president, board of directors and MIT faculty
- Salt: generally he spoke about an issue he's not an expert on, though he knows some of the people involved
- there are people who have said on multiple occasions he made bad comments but also let it go because he's done a lot for free software
- Salt: no next step, though it would be nice if we had a statement prepared if asked re: community and free software

## Code development progress
- wolftune: someone donated $80 recently, and also someone came (shak-mar) and fixed some things
- on a related note, there's a level of engaging with volunteers and sustaining the relationships
-  don't know how they found out about us yet. they noticed crowdmatching wasn't being run and helped fix
- with the commits in the repo, I think all that remains is to go into the server and run some commands to update the previous crowdmatches
- chreekat has been part of the commit review process so he's aware. if he hasn't run the crowdmatches in the next little while, we could check with him then
- Salt: it'd be nice to arrange a meeting where chreekat can attend again
- NEXT STEP (alignwaivers): check in with contributor, inquire how they found out about us and invite them to weekly meetings
- NEXT STEP (wolftune): if chreekat hasn't run the crowdmatches yet, reach out to him to find out how to do it
- NEXT STEP (alignwaivers): follow up to find a special meeting time with chreekat 

<!-- Decide where to capture outstanding tasks -->
<!-- Closing round -->
<!-- Capture TODOs -->
<!-- Add meeting notes to wiki -->