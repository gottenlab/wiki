<!-- Previous week's meeting notes: https://wiki.snowdrift.coop/resources/meetings/2019/ -->
<!-- Meeting location: https://meet.jit.si/snowdrift -->

# Meeting — December 16, 2019

Attendees: alignwaivers, mray, msiep, wolftune, iko, salt

<!-- Check-in round -->

## Metrics

Discourse (over past week): <!-- stats from https://community.snowdrift.coop/admin?period=weekly -->

- Signups: 0 -> 2
- New Topics: 1 -> 3
- Posts: 2 -> 17

## Reminders on building new best-practice habits

### Sociocracy stuff
- Drivers, describing driver statements: https://patterns.sociocracy30.org/describe-organizational-drivers.html
    - Current situation + effect + what's needed + impact of doing it
    - forum discussion of our use: https://community.snowdrift.coop/t/sociocracy-3-0-drivers/1309
    - Objections vs concerns
    - Example of proposal phase: https://community.snowdrift.coop/t/sociocracy-proposal-phase/1374

### Use of GitLab kanban boards
- Use the boards to pick your tasks, dragging or marking To Do and Doing appropriately
- e.g. wolftune's: https://community.snowdrift.coop/t/using-the-kanban-boards-in-gitlab-plus-wolftune-check-in/1263

### Conversation queuing
- "o/" or "/" means that you have something to say and want to be put in the queue
- "c/" or "?" means that you have a clarifying question and want to jump to the top of the queue
- "d" means thumbs up, encouragement, agreement, etc.
- three discussion mechanisms: hand symbol, round, open discussion

## Review previous meeting feedback
- smichel: could have been faster with amount covered, liked starting on time
- Salt: sloppier with the hand-raise, maybe *some* flexibility is fine
- wolftune: feedback for Salt, say "acknowledged" or "thanks for update" rather than "no problem" when there's no progress or otherwise there *are* problems
- align: I think it's easier to get sloppy when we have less on the agenda


## Carry-overs

```
## Salt met author of unfinished book on patronage funding at Communications conference
- NEXT STEP (salt): follow up with conference contacts, D. Yvette Wohn

## Volunteer recruiting application
- NEXT STEP (msiep): Wireframe of snowdrift.coop/volunteer-application (or wherever the form will live), potentially with second stage "fill out profile" option (see meeting notes 
https://wiki.snowdrift.coop/resources/meetings/2019/2019-11-18-meeting#volunteer-role-recruiting-etc )

## Advanced agenda planning for meetings
- secretary role, reminders about this etc.
- NEXT STEP (wolftune): create a thread to discuss how to resolve this tension <https://community.snowdrift.coop/t/planning-meeting-agenda-in-advance-secretary-role/1395>

## Issue grooming / dumping etc
- NEXT STEP (align_waivers): compile a list of references or links that explore the issue of grooming-guide
- NEXT STEP (smichel17): Delete or close the LFNW 2019 milestone <https://gitlab.com/groups/snowdrift/-/issues?milestone_title=LFNW+2019>
- NEXT STEP (smichel + wolftune + alignwaivers + msiep): schedule grooming session
- NEXT STEP (msiep): groom design repo issues

## Engagement / "Community member" recruiting
- NEXT STEP (Salt): create a driver statement for an active community voice role

```
<!-- Assign note taker -->
<!-- Confirm agenda order, inform if leaving early so as to not interrupt -->

<!-- New Agenda Down Here  -->

## Volunteer / role recruiting etc
- SeaGL volunteers, Civi status, board recruiting etc, follow up by mid-Dec with or without Civi
-some activity on github/gitlab posts from osu, currently will work with what's available
-NEXT STEP (wolftune): follow up w/ OSU on ETA, status etc.
-NEXT STEP (salt): reaching out to volunteers from SeaGL
- wolftune: don't consider blocked but mark it down to follow up with those volunteers soon
-NEXT STEP - (alignwaivers) - has spreadsheet of info from seagl volunteers, will send to salt (discuss how after meeting)
-wolftune:  other volunteers besides those from seagl? will backlog for now

## Code development progress / site building
- wofltune: chreekat replied via gitlab with the deploy command to run (and also showed me how to build a standalone binary). I still need to try it, send the binary to the server and ping him again when it's ready.
- wolftune: the 2nd step is documentation for build process of building the binary alone, and conferring with him whether the information should go in the code or the ops repo
- NEXT STEP (wolftune): build binary, send to server, ping chreekat [DONE]
- NEXT STEP (wolftune): document the new build process https://gitlab.com/snowdrift/snowdrift/merge_requests/173
- NEXT STEP (chreekat): deploy updated site, then after the updated site is deployed, run the backlog of crowdmatches https://gitlab.com/snowdrift/snowdrift/issues/169 

## Marking prominently that we're under-construction
- mray: there is a mockup ready, msiep and I are okay with it  https://gitlab.com/snowdrift/design/issues/111
- msiep: agreed
- Salt: what do we still need?
- wolftune: anyone with any capability with the code to look at the frontend code and hack on it. The practical part is figuring out who will do that.
- salt: haskell & html hacked together as a temporary solution - we need someone (with html knowledge) who can build locally
- alignwaivers: there were some webdev people from the last round in SeaGL
-wolftune: is there an implementation issue in gitlab? (not yet)
-NEXT STEP (mray): create a new issue in gitlab from mockup and put into implementation repo
- NEXT STEP (smichel17): Privately reach out to h30, make sure to mention the 'under construction' site banner issue
- NEXT STEP (salt): look through volunteers (recent first) to recruit someone to specifically work on this implementation1

## OSI Representative
- Salt: I met with OSI people regularly, they said we need a new OSI representative. They offered to help, but will need some back and forth to help
-wolftune: previous representative was Molly, but hasn't been in contact except in face-to-face
- Salt: we need a new rep, working on ehash
- wolftune will be primary contact, salt also backup contact
- NEXT STEP (Salt): discuss with ehash about OSI representation (and connect wolftune and ehash)

## Social Channels
- Salt: this came up with the static error pages and linking to social channels if the forum or main site were temporarily down. 
- Salt: what do we have, what are we using, what would we like to be using?
- wolftune: we're at snowdrift@social.coop on the fediverse, and twitter. We also have a placeholder sub-reddit /r/Snowdriftcoop
- Salt: what about facebook, instagram, anything else we should have?
- wolftune: we're sorta unofficially boycotting Facebook and holdings, not saying we should stick to nothing, also not sure if we should have anything on Google
- Salt: on the topic of an unofficial boycott, I'm for that, but there might also be value in having a page about it
- msiep: a note about it would be good
### round
- alignwaivers: as much as anti, may be good to placehold, may be good to generate momentum, maybe should be open even if temporary evil
- mray: not for FB and even social, we should lead by example, don't accept FB
- msiep: acknowledge dilemma, undecided about best strategy, personally I use it for groups/professional connection and discourage personal connection, no personal account; figure out how to have minimal exposure; hate the social pressure/lockin to use FB but accounts are harmful…
- wolftune: concerns about fb darkwebbing things, has had personal account since before they got as big as currently, use it to post anti-FB messages - want to maintain values of snowdrift, do not want to create content of value to stay there
- iko: accessing the walled garden makes sense to introduce people to snowdrift (who might not otherwise have yet heard of the project). However, there's also the optics of enabling people to stay locked in there, or as a floss project encourage more FB-exclusive activity (proprietary software, poses an access barrier). Basically agree with what others already said.
- salt: I use FB and hate it,  doesn't use other platforms, morally opposed to most of these platforms. in this case, in certain ways leaning towards usage of a placeholder - not allow further conversations, reference forum etc. what happens when we are on there and want to post something? value in having a placeholder, posting an indieweb type situation (as placeholder) might be useful. Don't want people to be posting in the walled garden, but want people in the walled garden to be aware OF the walled garden
https://indieweb.org/POSSE
### round over
- wolftune: fb has algorithm for burying posts that are pushing people to *leave* fb even via external links etc, people who are buried in their own 'newsfeeds' potentially won't see snowdrift posts anyway
- iko: instead of just having placeholder/shadow account, promote accurate information to negate misinformation. also skeptical if we'll even be found by others on fb unless gone viral
- mray: what's the point of saying we're here to be here, but we're not really here?
- msiep: if there's not a way of stopping conversations happening on fb, not a good placeholder. Need to weigh potential benefits. could be confusing to people understanding what we are about, because on the face of it looks contradictory
- salt: we don't have a twitter to have a twitter - we use it to direct people to mastadon / coop. content doesn't get posted there, get's referenced to other platforms
- wolftune: people may discuss snowdrift out of context if we don't have a presence there, not sure how much we can influence even if we did have a presence. being able to reach people in the walled garden is possible benefit as opposed to preaching to the choir
- iko: from a pragmatic view, if we have limited time/outreach bandwidth, we might want to focus on a smaller number of communication channels where we'll have the most impact. Seems fb won't be one of those channels at this time.
- wolftune: in building team, want to find people who already have some understanding
- alignwaivers: I'm not sure I feel comfortable about putting something negative (like being adversarial) about fb regarding generally having public enmity, but otherwise don't have a strong opinion
- NEXT STEP (salt): open discussion on forum continuing this topic https://community.snowdrift.coop/t/using-3rd-party-social-media/1403

## Capturing carry-overs, moving out of this "inbox"
- move this to next meeting



<!-- Open discussion? ~5min. if time -->

<!-- Decide where to capture outstanding tasks -->

---

## meeting evaluation / feedback / suggestions round
- alignwaivers: still practicing on note-taking, thanks for help
- mray: good meeting, extra round was helpful
- wolftune: time-keeping / time-boxing might be helpful esp. if we see a topic getting lots of discussion
- iko: good meeting
- Salt: likewise, not much feedback

<!-- ~~Closing round~~ Call this "Goodbyes"? -->
<!-- Capture TODOs -->
<!-- Add meeting notes to wiki -->