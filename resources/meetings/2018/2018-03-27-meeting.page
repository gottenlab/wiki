# Meeting — March 27, 2018

**Note: we also met 2018-04-03 but had no new topics of discussion, carried over the ones below.**

Attendees: align_waivers, chreekat, fr33domlover, iko, msiep, salt, wolftune

# Editors' note about carry-overs
- wolftune: I started planning meeting in advance, and so I went and deleted some no-longer-needed carry-overs below

# How to plan future meeting agendas
- marked in etherpad? what about sub-circle meetings e.g. outreach meeting?
- open issues in GitLab (or just topics at Discourse?) and then reference them in agenda items?
- NEXT STEP: open project-man topic in Discourse; assigned: wolftune

# Trusting people to follow categories on Discourse
- wolftune: i.e. how to post at Discourse and have at least as much trust it reaches the team as it did with old team ML
- relates to meeting reminder I posted, but I don't want to pollute discourse with things that get deleted as soon as people read them
- fr33: send an email?
- salt: there are two separate issues. first, where should we put meeting announcements?
- I would suggest posting in the team category and on irc, where irc is the open invite for people interested
- chreekat: sending emails can be both notification and reference, posting on discourse not really a notification
- +million to using IRC for chat-like notifications like meeting announcements
- fr33: agreed with chreekat, it's a notification. I have another idea, how about gnusocial or similar where people can get notifications?
- wolftune: I was thinking of notification separate from activity
- iko: unless the old setup wasn't working well, how about a direct mapping of the team mailing list to the team category?
- i.e. send out an announcement on the team mailing list about upcoming migration (to remind people and give them time to subscribe to the team category)
- thereafter announce meeting on the team category (and on irc right before meeting time for others interested to drop in)
- wolftune: yeah, I was asking whether we should do something like that or something else. if no one has objections, we will just do that
- NEXT STEP: send mailing list announcement mentioning team category on discourse

# Tracking concerns that aren't clear next-steps yet
- wolftune: e.g. <https://community.snowdrift.coop/t/noting-code-concerns-in-advance-of-design-work/622>
- maybe when in Discourse, just bookmark? if topics are brought up without next steps, what should we do?
- salt: clearly they weren't brought to next steps, then they should be discussed to arrive at next steps (whether that is at the weekly meeting or other channels)
- fr33: have a tag called "next steps" in an issue tracker and every weekly meeting, people go over them
- wolftune: that's a good idea, though the tag should be more "needs next steps"
- salt: tags sound like a great way to deal with this
- NEXT STEP: make Discourse tag; assigned: wolftune

# LibrePlanet
- salt: I did get a chance to talk to a number of people about snowdrift
- there's definitely some concerns, one item that came up was where we are legally
- my answer has been we have been working with a lawyer, however no paperwork filed for that
- wolftune: one thing to note, we will be a consumer coop, not a worker coop (which is more common in tech sector)
- the other thing is we're still an OSI incubator project (they're sponsoring the work for funding other projects)
- salt: another thing that came up, supporter rewards
- wolftune: if anyone is still missing their rewards, please tell them to contact us/me, but most have been sent out
- chreekat: can we finish sending those out? like, in the next month or so? don't let it linger, choose to send something out 3-4 times and then drop it
- just open a spreadsheet … 3 checkboxes "1st contact, 2nd contact, 3rd contact" and then it's done
- wolftune: that would be something that civicrm would help with. the outstanding ones are people who haven't replied with info, e.g. what size t-shirt
- it's like whether people rather I do other things than spend time chasing after some person who hasn't chosen their sticker
- salt: that's basically it, otherwise people were still very excited
- NEXT STEP: n/a


# LFNW
- salt: I spoke with Patrick, looks like OSI will have a table this year. wolftune, he said he sent an email to you within the last week
- wolftune: I didn't receive any email from him
- salt: do we have anyone else going to LFNW?
- align_waivers: I might be able to
- wolftune: I would like to, but won't be able to make it. if Athan might be able to go, I'll give him some stuff
- chreekat: I'd love to, but it's the other side of the country now
- salt: okay, Athan, if you're coming then we should probably coordinate this. I can't carry a lot of stuff
- NEXT STEP: follow up with OSI representative regarding snowdrift presence/table at LFNW
- NEXT STEP: test that emails are going through between wolftune and Patrick
- NEXT STEP: plan to get table stuff to LFNW; assigned: wolftune

# Databrary crossover
- chreekat: Databrary is an all-open source open science initiative. getting behavioural psychologists to share and reuse their primary source data, videos
- anyway, at the board meeting they talked about sustainable funding for the infrastructure
- obviously the federal grant model didn't really work, for all the reasons we understand so well
- so I spoke up (big room, lots of big wigs) and said "this is exciting to me because I'm working on a project tackling this very thing"
- there wasn't a lot of follow-up yet, but I'm sort of an in-house advisor for databrary, and there is the opportunity to influence possible collab
- one of the co-leaders of databrary (a psychology professor at Penn State) wants to hear more, as does one of their advisors (Phil Spies?), so stay tuned
- the more people who know people who might be interested = good
- I also chatted briefly with NYU's vice-provost of (?) talking about how databrary is potentially the seed of something much bigger, like the Library of the Future
- the interesting bit is that Databrary without its laser focus on ethics and privacy is just youtube
- but with its focus (mandated by all the complicated ethics concerned with having human subjects), it's charting a completely different path
- facebook coming from one angle, and when that implodes and people are picking up the pieces, here comes databrary from the other angle
- I mean that's pie-in-the-sky talk, but they are really tackling the hardest privacy problems, so everything else should be … cake?
- so I'm excited. questions?
- salt: no questions, but sounds interesting
- wolftune: maybe we can invite people from databrary to discourse and keep discussion going
- chreekat: I'll do that today in fact, can get one person up pretty easy
- NEXT STEP: n/a

# Pre-removal of carry-over
- salt: this was the question of whether to remove carry-overs prior to meeting
- even though they were your tasks, I think it would be nice for the group to see them checked off
- chreekat: I think maybe lining them up for knocking them out during the meeting is good, but actually knocking them out should wait for meeting
- wolftune: I'm fine with that
- NEXT STEP: n/a

# Any dev tasks?
- fr33: chreekat, do you have tasks you could assign to someone else? I admit I'm busy in general, reduce the bus factor if there's stuff you can let someone else do
- fr33: Stripe mocking? I need to make a merge request, what's the next step?
- salt: this was also another item that came up on LibrePlanet, "what can we work on?"
- chreekat: first off, <https://git.snowdrift.coop/sd/snowdrift/issues?label_name=%5B%5D=merge+request+welcome>
- besides that list, here really isn't much to be done
- we have the process, and the process is a pipeline, and the pipeline is not full. this is I think not indicative of a problem, other than general issue of team availability
- NEXT STEP: n/a (people inquiring should be directed to the "merge request welcome" tag on sd/snowdrift)


# Carry-overs

```
# Team use of Discourse, team introductions, welcome guides
- NEXT STEP: capture all the steps below as outreach issues
- NEXT STEP: team members post an intro about themselves in intro thread
- NEXT STEP: make a pinned post encouraging people to introduce themselves (some guidance about what sorts of things to mention)
- NEXT STEP: add pinned posts to welcome that are the guides to Discourse and community guideline links, FAQ…
- NEXT STEP: <https://community.snowdrift.coop/t/draft-please-join-us-on-the-snowdrift-community-forum/567>
- NEXT STEP: https://community.snowdrift.coop/t/draft-welcome-to-the-snowdrift-coop-community/568>

# Conferences coming up
- NEXT STEPS: get civicrm up before conference season

# Blogging
- NEXT STEPS: start Discourse topic documenting how-to-blog for team including starting drafts
- Assigned: wolftune

## CiviCRM project onboarding
- NEXT STEPS: describe a process for how we’ll use CiviCRM to enter and tag prospective first projects
- Assigned: Salt

## Recruit projects
- NEXT STEPS: consolidate our list of potential partner projects
- NEXT STEPS: GHC as incubator project? -> add idea to civcrm or wiki?
- NEXT STEPS: schedule a outreach rtc meeting to complete task
- Assigned: wolftune and/or Salt
```