# Meeting — April 24, 2018

Attendees: align_waivers, fr33domlover, msiep, smichel17, wolftune

<!-- Add agenda items below -->

## Vision/plan moving forward
- wolftune: we had confusion through chreekat's perspectives on workflow, moving from Taiga etc. about what "milestones" are, what's in them, how we use that stuff, but I think we *should* use them more. The things we need to deal with in the immediate future include:
* blog posts, state of project stuff and more
* outreach to community to join Discourse (semi-blocked mainly on just getting plugins going again)
* getting the core pages of the site functioning in a good-enough first version (dashboard stuff, snowdrift project page)
* recruiting Board
* outreach in progress to first outside projects
- NEXT STEP: explore project management workflow (milestones?) in gitlab [assigned: smichel17]

## Using Discourse more etc
- wolftune: discourse isn't quite ready to make the announcement, but reactions and some other setup has been finalized; ready to go once plugins are fixed
- also some existing tasks (writing welcome documents, sticky/faq, etc)
- feel free to post in discourse kind of like you would in irc, with assorted questions you might have
- NEXT STEP: team introduces themselves on discourse, starts using more

## What should MSiep work on next to minimize delaying what others can work on?
- wolftune: msiep, what's primary in your mind?
- msiep: most recently, notifications about dropped pledges (there's a gitlab issue). Did some backlog grooming a while back to look at again
- wolftune: I felt a tension around making high-investment things like video before launching.
- wolftune: some concerns about how to communicate that the budget is a control, but that you might want to increase it when you hit, not drop — relates to how-it-works page, video audio, dashboard
- msiep: this is somewhat a phrasing issue (which we can update at any time), wouldn't want to delay launch over that particular concern
- wolftune: elaborating, there's a concern about how the how-it-works visual communicates the limit (about how quickly you get "kicked out")
- msiep: Sounds like a good conversation for us two to have
- wolftune: status update from fr33? Is there anything blocking working on the dashboard?
- fr33: when you need something, I can spend time to work on it. Not sure if Robert is done working on dashboard yet.
- wolftune: There is a code issue for it? <searches> ==> https://git.snowdrift.coop/sd/snowdrift/issues/93
- fr33: It's better for me to have html rather than mockup image; I don't know sass well
- fr33: re: dashboard, I think a template already exists, iko made it.
- wolftune: do you have a link? ==> https://framagit.org/snowdrift/ui-prototype/tree/master/templates/page
- wolftune: we could work on improving this process; until now I wasn't sure what your questions/concerns/issues were.
- fr33: another issue: I wasn't exactly sure how the mechanism works, any document that explains it?
- wolftune: this is a good way to push us forward. You can post on discourse asking about the math, and then when you get an answer, that post acts as documentation for others
- smichel17: final note, I think Bryan'd be more willing to merge if others have signed off, so he doesn't need to have the responsibility for putting something incomplete in the code base.
- wolftune: ideally he wouldn't need to do that at all, just review the code side. Feel free to ping others (msiep, smichel17, myself) for reviews, too.
- NEXT STEP: msiep reviews design issues in gitlab
- NEXT STEP: clarify process for code implementation (maybe open Discourse topic), e.g. someone with html/sass knowledge working with someone with Haskell knowledge…

# Inviting others to meetings
- wolftune: does anyone have suggestions for how to invite people to meetings / communicate to volunteers who aren't already core contributors
- msiep: if they haven't been to a meeting before, whoever has had the most interaction with them is the best person to invite them, that'll make them the most comfortable
- NEXT STEP: post a discourse topic about this [wolftune/Salt]
- NEXT STEP: invite Jake to meetings

## Carry-overs


### GitLab issues
- wolftune: I've been using gitlab issues more, even for just tasks for myself, encourage others to do the same
https://community.snowdrift.coop/t/gitlab-tools-labels-kanban-board-milestones/712
- NEXT STEP: chime in on topic if opinions are had
- NEXT STEP: develop process for grooming / regular (weekly?) review of GitLab issues etc., assigned: wolftune

### LFNW
- athan and Salt are going
- <brief logistics check-in>
- Salt: Are promotional materials up to date?
- wolftune: They're good enough, updating not high priority
- NEXT STEP: wolftune gets Athan stuff

### Volunteer time commitements, expectations, checking-in
- wolftune: there's a tension around us all being volunteers, so it's difficult to "pressure" people to spend time on the project, but also some amount of bugging people can be helpful. 
- NEXT STEP: wolftune makes an etherpad for tracking people's hopeful-commitments
 
### Discourse admin stickies
- fpbot
- NEXT STEP: Salt opens Discourse topic on planning useful pinned topics

### Chreekat availability / Jazzy's departure / Road ahead
- NEXT STEP: n/a

### Trusting people to follow categories on Discourse
- NEXT STEP: send mailing list announcement mentioning team category on discourse

### Tracking concerns that don't have clear next-steps yet
- NEXT STEP: make Discourse tag
- Assigned: wolftune, smichel

### Team use of Discourse, team introductions, welcome guides
- NEXT STEP: capture all the steps below as outreach issues
- NEXT STEP: team members post an intro about themselves in intro thread
- NEXT STEP: make a pinned post encouraging people to introduce themselves (some guidance about what sorts of things to mention)
- NEXT STEP: add pinned posts to welcome that are the guides to Discourse and community guideline links, FAQ…
- NEXT STEP: <https://community.snowdrift.coop/t/draft-please-join-us-on-the-snowdrift-community-forum/567>
- NEXT STEP: <https://community.snowdrift.coop/t/draft-welcome-to-the-snowdrift-coop-community/568>