<!--

    Previous week's meeting notes: https://wiki.snowdrift.coop/resources/meetings/2020/

    Meeting location: Mumble server: snowdrift.sylphs.net port 8008

    Alternates for video/screenshare: https://bigblue.cccfr.de/b/mra-qy2-x6n or https://meet.jit.si/snowdrift

    See end of this pad for meeting-practices notes and reference

-->
 
# Meeting — July 29, 2020

- Attendees from last week: Salt, wolftune, smichel, msiep, mray, alignwaivers, Adriot
- Attendees (current): msiep, Salt, smichel, wolftune , Adroit

<!--

    Check-in round

    Assign note taker

    Assign time keeper

-->

## 1. Metrics
 
Discourse (over past week): <!-- from https://community.snowdrift.coop/admin?period=weekly -->
 
- Signups: 1 -> 0
- New Topics: 4 -> 1
- Posts: 29 -> 20
- DAU/MAU:  35% -> 31%

Snowdrift patrons: 123 <!-- from https://snowdrift.coop/p/snowdrift -->

## 2. Review previous meeting feedback
- wolftune: I appreciate timeboxes. Felt good that I've made progress between meetings
- adroit: regret missing first part, happy to see we're tackling the roadmap
- salt: thanks for all being here, timeboxing feels good. Need to think how to fix mray's issue cause we want him here.

    - TODO: Figure out how to respond when someone has connection issues

- msiep: appreciate hearing the things being worked out
- alignwaivers: roadmap is messy but also figuring out the process, +1 to timeboxing
- smichel: I like that we can all argue and not have drama / hold grudges, great team


## 3. Last meeting next-steps review

<!-- Between meetings when done, mark each NEXT STEP as [DONE] or [CAPTURED AT <LINK>] -->

### formal invite to adroit 
- NEXT STEPS (wolftune + alignwaivers + adroit): onboard adroit and document steps taken [CAPTURED] <https://gitlab.com/snowdrift/governance/-/issues/70>

### Weekly meeting day/time 
- NEXT STEPS (alignwaivers + smichel): update lists forms, posts with the meeting times Wednesday 1PST, Fri 1PST [DONE, I think]

### Reaching out to potential funding sources (Salt) 
- NEXT STEP (alignwaivers + Salt): draft email

### Finalize Roadmap
- NEXT STEP (smichel): Capture updated roadmap at <https://wiki.snowdrift.coop/planning> [DONE]
- NEXT STEP (smichel): Clean up roadmap planning etherpad <https://snowdrift.sylphs.net/pad/p/roadmap> [DONE-ish]
- NEXT STEP (smichel): Have 1:1s (post-regular-meeting) to claim items for Q3.2 [Hopefully after Jul 29 meeting]
- NEXT STEP (smichel): Ask chreekat if he'll take point on OSUOSL transition [DROPPED]


## 4. Current Agenda

<!--

    One minute silence, check thoughts and notes/tasks/emails to surface tensions, add agenda if appropriate

    Confirm agenda order and timeboxes (in 5 minute increments), adjust for anyone who must leave early

    TEMPLATE for agenda items:

### TOPIC TITLE (LEADER) <!-- Timebox: XX minutes (until hh:mm) -->
<!-- -->

### (carry forward) Reaching out to potential funding sources (Salt)  <!-- Timebox: 5 minutes (until hh:18) -->
- Salt: This isn't a priority for me right now / I can't be the point person on it. I can help, but can someone else take it?
- wolftune: 
- NEXT STEP (wolftune): Follow up with alignwaivers re: draft email

### Crowdmatching mechanism update (wolftune) <!-- Timebox: 10 minutes (until hh:24) -->
- wolftune: I want to give an initial update and maybe some open discussion. No concrete goal, jsut update.
- wolftune: Focus with mozilla lead to thoughts about framing and discussion about *potential* mechanism shifts.
- wolftune: I'm optimistic about them
- wolftune: Goal is to consolidate our thoughts, share with others clearly/consicely instead of wasting time sharing full discussion.
- wolftune: Core issue: People not intuitively understanding "more patrons => more $$". Believe this is related to open-ended-ness.
- wolftune: Simplest version of new update: State goal up front (say, 10k patrons, $100k), after which matching turns off. Resolves lots of tensions around budget limits.
- wolftune: Just stating the goal makes messaging much clearer.
- wolftune: Option for multiple goals / milestones to balance ambitious and modest goals.
- Adroit: I'm not totally clear on what the proposal is -- getting rid of matching? It seems important for the success of the platform…
- wolftune: It's not a proposal at this point, just brainstorming.
- wolftune: Core part of matching stays intact
- Adroit: Isn't there a problem of no more incentive to donate once a project hits their goal?
- msiep: If the crowd drops below the goal, crowdmatching kicks back in.
- msiep: Getting to the goal doesn't create an incentive to drop out. The original motivation is: "I'm willing to donate, but only if a bunch of other people do to." Getting to the goal is basically that happening.
- Salt: I don't think I want to get into the details for this meeting. I'm happy that we're staying open to discussion like this but not yet convinced.
- wolftune: If a project has a goal after which crowdmatching turns off, there is an effective limit per project, so budget limit is no longer needed.
- smichel: this is just the smallest of several tweaks that are possible
- NEXT STEP (wolftune): lead asynch work on clear criteria list for the model, 


---

## 5. Open discussion <!-- Timebox: 5 minutes (until hh:31), cut off 5 minutes before meeting end -->



## 6. meeting evaluation / feedback / suggestions / appreciations round
- Adroit: appreciations of finishing early, also uncomfortable with wondering if we got to everything
- smichel: Happy it was fast
- Salt: Happy everyone's still here, ??? robert, would like to have a way to move through open discussion
- wolftune: Missing a few people, first time at the new time but we should figure out why alignwaivers & mray aren't here


<!--

    Meeting adjourned!

    Clean up meeting notes, then add to wiki

    Prepare this pad for next meeting:

    Replace previous meeting eval notes with new and filter it down to essential points

    Move agenda notes up to "last meeting next-steps review" section

    remove everything except the topic title (without the topic leader) and the NEXT STEPs

    unbold the topic headers

    Update next meetings date, clear attendee list

    Update old metrics, update date, leave new blank

    Clear authorship colors

-->


<!--
## Meeting best-practices and tools

### Etherpad use
- Use chat in etherpad (and add your name)
- Option: audio notifications on firefox via https://addons.mozilla.org/en-US/firefox/addon/notification-sound/
- Option: Bookmarklet to make the chat bar wider. Consider making a bookmark (select the whole line below starting with "javascript:" and drag to bookmarks bar). Adjust the width in pixels by changing "280" .
javascript:(function () { const width='280'; const box = document.querySelector('div#chatbox'); if (box) { box.style.cssText=box.style.cssText+' width: '+width+'px !important;'; } const pad = document.querySelector('iframe').contentWindow.document.querySelector('iframe').contentWindow.document.querySelector('body#innerdocbody.innerdocbody'); if (pad) { pad.style.width=(document.body.clientWidth-width-50)+"px"; } })();

### Agenda topics
- Each topic facilitated by topic lead with main facilitator help
- As needed, ping !folks on matrix to read over anything advance, ideally before the day of the meeting

### NEXT STEPS
- each topic should capture NEXT STEPS (or clarify that there aren't any)
- should be clear and actionable
- assignee agrees to capture or complete by next week

### Timeboxing
- timebox each topic, rounded to nearest 5min., settled during agenda confirmation
- format is: "Timebox: 10 minutes (until hh:mm)" (in an html comment so it doesn't appear in note archives)
- at topic beginning, convert the :mm to expected end time
- at timebox end, "thumb polls" may add 5 minutes at a time
- hand symbols
    - "^" approve, extend the timebox
    - "v" disagree, move onto the next topic
    - "." neutral

### Discussion options
- open discussion
- call for a round ("pass the mic" style, facilitator makes sure no one is skipped)
- hand symbols
    - "o/" or "/" means you have something to say and puts you in the queue
    - "c/" or "?" means you have a clarifying question and jumps you to the top of the queue
    - "d" means thumbs up, encouragement, agreement, etc.
    - ">" means you understand someone's point, please move on
    - "d>" indicates feeling complete on an agenda item, ready to move on to

### Notetaking
- "???" in notes means something missed, please help capturing what was said
- aim for shorthand / summary / key points (not transcript)
- don't type on the same line as another notetaker ( ok to do helpful edits after but not interfering with current typing)
-->
