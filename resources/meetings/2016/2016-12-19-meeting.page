# Meeting — December 19, 2016

Attendees: iko, mray, msiep, salt, wolftune

## Checkin

- iko: did some Taiga cleanup, sorting old tasks
- mray: been busy outside of snowdrift, nothing to report
- msiep: opened MR with some wording and css changes, waiting on chreekat's
  comment, also mray/wolftune re sign up vs log in
- Salt: finished grad application, less busy hopefully, looking forward to
  doing more snowdrift stuff
- wolftune: things have been crazy, but keeping up with answering questions on
  irc

---

## Agenda

- SSO MR status (requested by fr33domlover)
- Holacracy elections (Salt)
- Finalise intro audio script: next steps (iko)
- mray/wolftune's feedback re: sign up vs log in for MSiep's merge request
  (MSiep)
- email to dev team (Salt)


## SSO MR status (fr33domlover in absence)

- fr33domlover (from irc): i can't join [the meeting today]. someone please
  raise the SSO feature thing :P
- fr33domlover (from irc): especially for chreekat to update on its status as
  the person who merges the MRs eventually
- iko: last I heard there was some complication writing tests for Discourse
  that called for developers to install a Discourse instance
- jazzyeagle had asked chreekat and on the dev ML for advice on writing tests,
  to move the MR forward
- chreekat replied on the MR today that he'll look at this and other MRs
  tonight
- Salt: it probably doesn't need a Discourse instance, a fake responder
  returning Discourse parameters or similar will do
- this is a dev issue, and there is no one representing the dev team here at
  the moment

Next steps: fr33domlover/jazzyeagle emails chreekat on how to proceed in order
to get MR accepted


## Holacracy elections

- Salt: iko, are you okay with continuing to handle meeting stuff until
  elections?
- iko: it's fine (not that I'm really filling the role, just taking notes, no
  interpretation of Holacracy docs)

Next steps: speak with smichel17 to host elections


## Finalise intro audio script

- Salt: next steps, schedule meeting to finalise script?
- wolftune: I'm flexible for meeting time, though with distractions throughout
  today and tomorrow
- Salt: did you want some private time before meeting to go over feedback?
- wolftune: I'm not going to insist on that
- Salt: how about meeting sometime later today?
- MSiep: I won't be able to join today, and I would like to join the discussion
- I'm available tomorrow after 1 PM PST
- wolftune: I'm not sure about tomorrow, parents' moving truck coming around
  that time
- mray: I probably won't be able to join tomorrow, family's arriving
- Salt: it would be nice to work on this before Christmas
- sounds like tomorrow's out, with moving truck at wolftune's and mray busy.
  how about Wednesday?

Next step: meet on Wednesday at 22:00 UTC to finalise script


## mray/wolftune's feedback re: sign up vs log in for MSiep's merge request

- MSiep: there's been some discussion on [MR
  31](https://git.snowdrift.coop/sd/snowdrift/merge_requests/31), particularly
  about register and login pages
- before my MR, the form labels on the register and login pages are identical.
  in the login passphrase field has been changed to be less verbose
- the current forms are coded so that the field labels are identical on login
  and reset-passphrase, which I see as a problem
- mray, you mentioned the register page should remain more verbose?
- mray: the register page can be verbose, to be more welcoming, but if you
  think there is advantage to being brief, I understand that. no hard feelings
- MSiep: if you can reply to wolftune's comment and say okay for now as a
  solution, then the only thing left is chreekat's comments for the haskell
  side
- mray: sure
  (<https://git.snowdrift.coop/sd/snowdrift/merge_requests/31#note_451>)


## Email to dev team

- Salt: my concern is we may lose development momentum. while we have
  fr33domlover and jazzyeagle doing some dev work, because chreekat is no
  longer doing dev full-time
- should we sent out an email beyond the one discussing SSO? just a "here's
  what it's going to look like now that chreekat is no longer spending full
  time on this"
- so people know MRs might take longer, for instance. what we're looking for
  specifically
- wolftune: I think we should confer with chreekat on having him send out the
  email
- Salt: this is more about what items to include in the email
- wolftune: that makes sense, to pass on to chreekat for his announcement
- Salt: major blockers and timelines, e.g. css stuff
- wolftune: I'm not sure about mentioning css stuff, or what the status is on
  it
- it would be good to ask him about availability, but he might still be
  figuring that out
- Salt: it would be nice to invite some developers to work on the codebase

