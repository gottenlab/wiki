# Meeting — October 24, 2016

Attendees: mray, MSiep, iko, wolftune, chreekat, fr33

---

## Checkin

* mray: all right, making slow progress looking for animation software for intro
* MSiep: fine, busy week,
* iko: nothing to report this week, waiting for the dust to clear for UI integration
* wolftune: fine, need to catch up on status of everything.
    * I would like to get up-to-date about the status, also have some concerns
    * looking to better communicate status publicly
* chreekat: been coding, I finished a prototype last 1.5 weeks ago, but still a lot to do to get ready for production
    * there's still 1 thing left to figure out, the rest is execution. it's going well overall
* *fr33 is listening in and has no checkin comment*


## Agenda
- UI integration (chreekat)
- Publishing status to public (chreekat)
- Q&A checkin with chreekat about things (wolftune)
- Outstanding item: legal address change and other legal matters (wolftune)
- Intro video (wolftune)
- Getting other volunteers (chreekat)


## UI integration

- chreekat: since iko mentioned it, I just wanted to clarify expectations
- there's still some amount of other coding left to do, which is why it's not in "mvp"
- not sure when I'll get to it yet, as there are some high-priority things to cover first


## Publishing status to public

- chreekat: <https://git.snowdrift.coop/sd/snowdrift/network/split-mechanism>
- the gitlab activity page is probably just as good
- mray: maybe <https://git.snowdrift.coop/sd/snowdrift/graphs/split-mechanism> ?
- wolftune: the question is, where's a prominent place where we can check our actvity?
- we don't want to make it hard for people
- chreekat: tell me which page to put it and I'll put it in the backlog
- wolftune: I think that's a good step. we could also post to social media (gnu social, etc.) but that's more Salt's area
- even a running page on the wiki is even something
- wolftune: feedback from the design folks?
- mray: the graphs are easy to parse. while not helpful, it might be a good idea to use our own project page, not on the front page or anywhere else
- chreekat: looks like there's a bug with the graphs after 6k commits
- fr33: dev blog? how does chreekat feel about maintaining one? (nobody else can...)
- mray: wasn't there a plan for a blog?
- wolftune: we moved it to the wiki
- iko: which works for the most part, only thing being, the wiki wasn't visually designed for a blog.
- I was thinking about a blog too (e.g. hakyll), but not sure if devops has resources for it
- chreekat: we had a discussion about this a while ago, but yeah, the problem of maintenance
- wolftune: we should have a blog post on the current status, better than no updates in the status quo
- fr33: in the early days of Devuan someone would make a weekly summary and it wasn't any
  of the devs, just someone who tracked the work and volunteered to summarize things (they just looked in ML)
  chreekat: exactly. I think I need to make more updates to the dev ML
- under <https://wiki.snowdrift.coop/blog/dev>. to emphasise, anything is better than nothing
- chreekat: I know what I'm doing currently into the next week. except for me working on the backend, there's mockups and prototypes made of new UI, I don't know what else is going on right now (if there's anything going on)
- JazzyEagle is working on (account migration on) the production branch (I may need to help with that)
- we should go back to taiga, that's where we do our tracking. I haven't been on taiga recently, but, we should try
- chreekat: it's too bad smichel17 got busy, he was doing much of the project
  management stuff
- wolftune: yeah, it's hard and I got busy as well
- chreekat: let's give you time to try, and if you are unable/haven't time to do so, we find someone to do it
- wolftune: is there anyone else who would like to do that?

- Next steps: iko records meeting notes, wolftune will transfer action items to taiga and update the blog on project status


## Q&A checkin with chreekat

### Development status and Stripe API

- chreekat: did you have any specific questions?
- wolftune: generally, the status of things. what are the high-priority things you are working on?
- *chreekat shares screen with terminal*
- chreekat: project page (right now there's only one project it's also on the dashboard), payment info page
- wolftune: is the pledge button visible all the time?
- chreekat: the member will have to add their payment info first
- wolftune: so the pledge button doesn't show unless you have payment info? it's functional and good enough for now
- *chreekat demonstrates add payment info with Stripe test and API credentials*
- fr33: are you using stripe's JS or HTTP API?
- chreekat: both. e.g. on the payment-info page, there's a js button.
- for doing payments, stripe populates the form and gives us a token to remember this card, so that's http, and http to do payments.
- wolftune: what's the status of tests to ensure all the calculations are correct?
- chreekat: there are some db commands available, you pass a command to run some db action. you can fetch patron history, crowdmatch history, etc.
- wolftune: so you can look up the payment history over time
- chreekat: yeah. I've been working on tests on these things
- run certain actions to ensure data is okay, working on redirects. I need to test the amount that the person is charged with is correct.
- I still need to move the crowdmatch functions from prototype and tests, pass -Werr, add constraint that the stripe token are linked, add manual migrations, automate test setup (right now setting up things in shell before calling test)
- make sure API for Haddocks look all right, add some tests that integrate both website and crowdmatch library (make sure website reflects crowdmatch changes for user)
- I might talk to the Stripe people on IRC, they have employees there
- wolftune: it's good to have this Q&A so you can talk about priority items

### Volunteers

- wolftune: what are your thoughts about reaching out to volunteers? I understand you've been busy with code, and would probably need to set aside a day to coach volunteers as they join
- chreekat: I need to rely on advisors more
- wolftune: what about code review?
- chreekat: that's something I could use
- wolftune: as a suggestion, you could think about doing something to keep the most valuable interested, to make sure they're aware of what's going on with the code and they're around when you need them
- chreekat: after this is done in 3 weeks, there are a lot of loose ends to gather. until then, it's hard. currently, the code is not ready, though it's possible to carve out sections
- wolftune: yeah, we don't want to waste people's time. however, you could send sections of code to people for feedback, even in half-baked state, to give people a chance to connect and respond
- chreekat: excellent, I will do that

### Security

- mray: I just want to bring up a concern users may have -- where are we on security? security is always going to be an issue, but what is our current situation in terms of accepting other people's money?
- chreekat: we have a db of email addresses, there are two people with access. I'm the only one who goes in. the other person is wolftune
- we don't store cc information and never will. people are talking about getting rid of proprietary js and sending cc info ourselves.
- I don't want to do that, it's a lot of risk. even if we do that, it won't be stored/cached.
- wolftune: potentially a MITM issue
- mray: what about the current status/implementation?
- wolftune: we don't touch it. we want to avoid it, but the way to avoid it needs proprietary js
- mray: are there other attack vectors?
- chreekat: there are API keys (it's how we interact with the Stripe API). those are not in the db, but are on the filesystem
- the other vector is password cracking. we have a library that handles passwords, I'm fairly confident about it, the person who maintains it is a crypto expert.
- it would be nice to have someone devoted to studying security issues, but I'm making do with what we currently have
- TLS used throughout
- mray: would Stripe be able to shut us down, as the only payment processor?
- chreekat: yeah, it's possible. that's a reason to plan for multiple processors.
- mray: suppose we have a security incident. do we have measures of how to react to that?
- chreekat: again, that's something the person in charge of security issues to
  implement
- chreekat: although Stripe could shut down our account, it's a low possibility. We do have a positive relationship with them.
- there's old emails. I have personally emailed a Stripe employee who maintains a Haskell library.


## Closing round

- wolftune: to summarise, we touched upon volunteers a litte earlier in the discussion the other agenda items can be discussed next time.
- regarding legal address change: I need to deal with that ASAP. this basically means calling our lawyer to figure out what to do
- I will be visiting relatives in the 2nd week of Nov (Nov 5) for a week, but I'll be around on IRC or can chat on Jitsi if needed
