# Meeting — November 10, 2017

Attendees: Salt, iko, wolftune, rachelfish

<!-- Agenda -->

## SPECS-STORIES process

- wolftune: need bryan/robert for deeper conversation, but also looking for others' comments/feedback here
- chreekat's idea — SPECS-STORIES.md should reflect already implemented features
- proposed process:
    - discussion to get consensus, tying mockups to US
    - open issue to discuss what US should be for a feature, come to consensus
    - if it's something we want then make it available for dev

NEXT STEPS: wait for other stakeholders to be present to discuss further


## When to announce working pledges

- Salt: I agree with mray that having site with UI ready to launch is important before we publicise launch, We only have one shot at it, and I want to make that shot count
- wolftune's suggestion: talk to mray and MSiep to confirm site is ready before alpha launch announcement

NEXT STEPS: create/update issue to make sure people know whose opinion we're waiting on


## Iterating / deploying etc.

NEXT STEPS: deferred (until chreekat/devs are present)


## Helios

- iko: is Helios install still planned? <https://tree.taiga.io/project/snowdrift-ops/issue/542>
- wolftune: this is software related to running co-op and board of directors
- it's the software OSI uses to elect their board. best way to run online elections with balance between privacy/security
- we need to have official board, short-term no need to do anything yet (this opens up a lot of issues I've been avoiding about boards/elections)
- long-term we want to use something like it or this software

NEXT STEPS: no short-term action required


## Migrating email to a 3rd party

- Salt: we're running a 3-year old OS that's nearing EOL. this means missing features, filesystem unsupported for production use
- we also currently self-host email. not possible but difficult without dedicated sysadmin
- chreekat doesn't want to move them due to considerable migration time needed, but I think it's a medium-priority task at this point
- don't know how to move forward and chreekat doesn't want to deal with it
- Salt's suggestion: reach out to 3rd-party, Kolab, to start migration process. wolftune has contact with them.

NEXT STEPS: wolftune to reach out to Kolab sometime


## Discourse

- Salt: categories have been set up, SSO is enabled. we need to tell people to start people to use it and to commit to using it ourselves
- wolftune's suggestion
    - potentially ask 1 person to try sso
    - would like to include in announcements
    - anyone we could trust is welcome to be moderator

NEXT STEPS: start using it


## Non-meeting check-ins

NEXT STEPS: no next steps


## LibreJS payment processing

- rachelfish: what will it take to get LibreJS payment processing?
- wolftune: since balanced payments shut down, the plan is to do something similar what crowdsupply does, don't store credit card info
- there are some liability items to do, then we have to build frontend to replace
- rachelfish: if you store credit card info, then you'll need to go through PCI compliance
- any reason why you can't write js to replace Stripe js?
- wolftune: pj from Protean project wrote js to replace proprietary js, so it exists, but just writing that brought it all the PCI compliance issues
- 1 charge to multiple projects works with Stripe API, not sure
- if you're interested, maybe reach out to crowdsupply and other people

NEXT STEPS: wolftune will email rachelfish info from pj and crowdsupply etc.


## Meeting day/time

- wolftune: have heard from iko and MSiep on whenisgood meeting time poll
- smichel17 and chreekat emailed saying schedule too variable at the moment, chreekat's schedule may stabilise soon
- from the responses I received, Mondays and Wednesdays seem to be the best time

NEXT STEPS: decide and announce new meeting time by Sunday (Nov 12)


## Tool for managing outreach tasks

- Salt: Taiga has a lot of stuff. I don't think gitlab is good for dev, but not outreach
- discourse may be the correct place

NEXT STEPS: Salt decides this stuff, pings wolftune


## Initial Board

- wolftune: board should have president, secretary, treasurer, etc. and a board of directors to make decisions about significant legal things
- at the moment there's just me, which is awkward and uncomfortable
- hesitation with talking to lawyer due to potential cost involved
- rachelfish: relationship with smichel17's governance docs?
- wolftune: the initial tasks of the board is covered partially in the governance docs, but is bigger than day-to-day accountabilities
- wolftune's suggestion: 
    - appoint an initial board
    - seek advice from OSI and other orgs
- Salt's suggestion: 
    - have a board discussion meeting, make sure volunteers over the last year with core team present (everyone is welcome to attend, but team needs to be present) 
    - have an agenda beforehand
    - at the end of the meeting, have a proposed board. proposal will be announced with a week for comments

NEXT STEPS: 

1. plan board meeting, set available time, contact everyone
2. gather list of resources and advisors to contact
3. reach out to advisors with reasonable set of questions


## Previous items completed

Item: ping chreekat about adding space, that will enable Salt to do other things he can't handle now
Assigned: Salt

Item: put up Ghost docker/launch Ghost, enable Discourse SSO
Assigned: Salt


## Carry overs

Carry over: update on the video  
Assigned: wolftune: record final audio and compose music

Carry over: legal email, discussion, etc. from https://wiki.snowdrift.coop/resources/meetings/2017/2017-08-11-meeting#next-steps-3  
Assigned: wolftune

Carry over: Holacracy thoughts: smichel: make report about what we’ve got so far, what seems to be working, and what we would need to do to go further  
Assigned: smichel

Carry over: prioritizing iterative work process / dev  
Assigned smichel will document this process better: find tasks: Kanban → US → internal tasks for the US

Carry over: code policies, specs for code modules  
Assigned: fr33 will talk to chreekat, get him to document his criteria/policy for code merging and onboard other maintainers; also mechanism specs written out

Carry over: Discourse launch  
Assigned: Salt: back up existing Discourse to allow safe testing prior to full launch/use

Carry over: add notes from wolftune's feedback to Salt about his SeaGL presentation to appropriate wiki page with presentation resources/ideas  
Assigned: wolftune

Carry over: wolftune and Align_Waivers book Bolt Bus and arrange place to crash, talk to Salt if need help reaching out for options  
Assigned: wolftune

Carry over: Salt describes a process for how we'll use CiviCRM to enter and tag prospective first projects  
Assigned: Salt