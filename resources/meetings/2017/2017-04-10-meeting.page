# Meeting — April 10, 2017

Attendees: iko, MitchellSalad, mray, MSiep, Salt, smichel17, wolftune

---

## Agenda

### Docker-Ghost update

- iko: I have a skeleton docker script done, but it will need a bit of tweaking later (to import data and other settings) because we don't have an existing ghost instance
- demo instance: https://snowdrift.sylphs.net/blog
- I can email invites to people interested in testing it (Ghost doesn't allow setting up accounts without emailing people)
- a couple questions: what will be the blog url or subdomain?
- Salt: blog.snowdrift.coop
- iko: what are we using for smtp server?
- Salt: mx1.snowdrift.coop (postfix, but for docker you should set up ssmtp and direct to mx1)
- iko: not sure yet how to import existing Markdown posts with author attribution and date? create accounts for team members, create new posts and backdate them?
- wolftune: don't worry about it, just post something that says "we have a new blog, here. All the old posts are on the wiki, here."
- iko: okay. do we have a blog design yet? if we don't, I'd have to check with mray on plans
- mray: not yet
- smichel17: we can make this snowdrift-like with a few adjustments to make it basically usable (same banner and fonts)
- MitchellSalad: yeah, the default theme is pretty nice
- **Next step: iko sends invites for demo instance**


### LFNW planning

- wolftune: any news about the table?
- Salt: not yet, but I can check again
- wolftune: I would like to make this a milestone and focus on this
- Salt, chreekat, myself and a couple other people will be going
- for upcoming things, there's the video and discussing plans for those who are going
- Salt: we can get a room for 6 + 1, not sure if there are bike racks
- wolftune: I need to confirm with other people, but we can talk more about this offline
- Salt: I hope to find time for CiviCRM
- I'd need to sort CiviCRM contacts by the time you added them back in Apr/May
- wolftune: what would be reasonable timeframe and priorities to aim for?
- Salt: I'd like to hear more about the agenda items before deciding on this
- **Next steps: Salt and wolftune to work out logistics, everyone to think about what milestones are reachable**


### Video

- wolftune: we're dropping the idea of specific example with numbers and talk about the concept of matching in general. for concrete example, go to the how-it-works page or the rest of the site
- the challenge is, currently we have an animation that describes how it works and what happens when going above the match
- we sort of have the 2nd 1/2 or 2/3 to finalise now
- michel17: is there any place I can go to see the progress?
- mray: there's a version on Seafile, not the latest
- Salt: of all the time, hours and hours, we're rewriting it again?
- wolftune: yes, my apologies. we were told by the visual side they would like a finished audio, but now that we're working on the visuals, we have concerns about the audio, so we're going to change some parts
- mray: that's not what it looks like from my perspective. that's why I was hoping to get the final audio first
- I'm waiting for audio to continue with the video
- wolftune: I understand Salt's frustration on spending a lot of time on exact words, only to throw out entire sections
- I don't think there's a blocker at the moment
- JazzyEagle: if we already have sections finalised, can we create audio for the parts that we like and let work proceed on those?
- wolftune: we have the 1st half done
- smichel17: is there a misunderstanding on what "final" means?
- mray: final is the version that is going to be shipped with the video. I
  think this is currently the normal flow. we will have a refined version
  of the script that may need some adjustment during the video animation
- wolftune: we tweaked wording with the expectation that this was the final version. however, if we expected changes later, we wouldn't sink time into those details
- Salt: we won't have a final text or video, so let's move on to other items
- any next steps that need to be accomplished right now?
- wolftune: I think we should get clarity on what to expect going forward, I'd like to discuss this after the meeting with everyone interested
- mray: the next step will be filling the missing gap with the audio
- **Next step: wolftune, mray, and anyone who wants to chime in have a separate meeting to create some alignment here.**


### Merging the alpha-ui etc.

- Salt: what would it take to get alpha-ui branch to master? a question for people who had experience with the alpha-ui branch
- JazzyEagle: everything is good to go except the funding mechanism => no functional dashboard. It's a question of whether we want to wait until we find someone to finish that or merge without.
- Salt: chreekat pushed a crowdmatch-utility branch implementing the
  rest of the mechanism?
- MitchellSalad: I think it's a work in progress
- **Next steps: Build the alpha UI branch, compare it to tasks in Taiga.**
- **Next steps: Stephen will look into what's needed here.**


### More regular check-ins / meetings?

**Next steps: Check group availability chart (make sure yours is up to date!) and find times to meet with people**
**Next steps: wolftune and smichel can chat about this**


### backend stuff

- MitchellSalad: I would like to ask what I should be working on? what are the higher priorities?
- I can put in a few hours/week, or who to ask? should I be asking chreekat or wolftune?
- wolftune: I'd suggest asking chreekat, everything backend is chreekat's domain
- MitchellSalad: are there people, programmers, who have expressed interest, hang out in the IRC?
- I'm happy to help people along, if they're just not sure how to get started
- wolftune: that would be great
- iko: MitchellSalad, from a design perspective, getting variables that can be used on the dashboard would be great. not sure how high a priority it is for dev, but this would be a big step for moving frontend design forward
- MitchellSalad: any other Haskell devs?
- Salt: fr33domlover and cgag have been active
- **Next steps: Continue to discuss this out of band.**
- **Next steps: Get backend/mechanism stuff into taiga**


## Afterwards

### Find a time for smichel17 and mray to chat

- smichel17: I got a page full of feedback from LibrePlanet that I'd like to share
