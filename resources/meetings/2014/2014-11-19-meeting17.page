# Meeting 17

*Wednesday, November 19 2014, 7PM Pacific (3AM UTC)  
phone chat on freeconferencecall.com (605) 475-4000 pin 564511#  
backup alternate number: 951-262-7373*

live-recording of minutes on [etherpad](https://snowdrift.etherpad.mozilla.org/1)

## Background readings / updates

* Look over [fund-drive](fund-drive) and [fund-drive-video](fund-drive-video) pages
    * esp. the link to the [etherpad script](https://snowdrift.etherpad.mozilla.org/12)
* Look at the draft of the [fund-drive site](https://snowdrift.tilt.com/launch-snowdrift-coop)

### Assigned tasks from before

* Kim: investigate Loomio
    * *Actually, Aaron ended up getting more involved there, prompted by Mitar*
* Aaron: start process of Open Hatch signing up, draft letter, clarify things; site work as feasible, follow-up with more connections etc.
    * *Didn't get finalized, but chatted with Asheesh at Debconf, things seem positive but still tenuous*
* David & Mitchell: coding etc.

## Agenda

Discuss realistic launch time-frame for fund-drive and priorities / issues / prerequisites.

* Concerns, some maybe essential, some can work on improving after campaign launch
    * Need for concrete goal, not perfectionism, push forward ASAP
    * Lack of projects signed-up that would otherwise show diversity and credibility and concrete sense of things
        * Would have been ideal to have succeeded in getting projects signed up, but we haven't yet
        * *Could* delay fund-drive until after we get some projects listed…
        * Or *perhaps* we at least aim to have the sign-up process *clear* enough and truly robust enough (ideally finalized forms and an intake process) so that we can intake projects during the fund-drive and be able to announce news about that as part of updates
    * Need to finalize and test e-mail notifications for all sorts of things on the site, especially the blog so that we can engage fully with people who discover the site from the fund-drive promotion
        * Also: discussion pages for blog
    * More appealing home page design
        * a clear button pointing to the fund-drive
        * More focused visuals, less blocky text stuff etc.
    * Trust-markers, i.e. endorsements from known people and orgs both or either for the campaign or just on the site
        * Related: making past team-members visible on the site and also moving some current to becoming past
    * A truly good enough video that gives positive impressions
        * Get a diverse group of people to appear in the video ideally
        * Multiple videos ideally
    * More concrete explanation of stretch goals / features that further funding will support
    * More clear graphs / testing the user experience for pledging and assuring it is a positive experience
    * Plans for press / promotion (overall and whether specifically a period prior to campaign launch)
        * Besides building anticipation, maybe important to address concerns and glitches and questions before we're committed to the fund-drive process

## Next steps assigned

* David: Keep plugging away at the website, set priorities. Participate in video
* Mike: Excited to have a deadline. Can't do much other than give feedback on video or reward levels.
* Aaron: Make the video happen, finalize other fund-drive elements
* Nikita: continue coding
* Sam: Look around website, help with video and fund-drive presentation / writing.
* Kim: Try to find a friend with a decent camera and record a segment of the video. Go through script on Thursday night.

## Summary

We will aim to make fund-drive active by Tuesday November 25! Go!

### Minutes

* Tested meet.jit.si yet again, actually got a meeting started but could not get everyone's video and audio working at the same time.

Misc. updates:

* David has left his day job and he and Nikita are working hard on the backend.
* Sam, who has experience with wikis and the Debian project, has joined the committee to help with organizational, documentation, and legal issues.
* Snowdrift.coop profiled in online version of Linux Pro magazine, will also appear in upcoming print issue.
* Session proposal has been submitted to LibrePlanet March 21-22 2015.
* Discuss realistic launch time-frame for fund-drive and priorities / issues / prerequisites.
    * Concrete goal: Discussion as to reason for $3000 goal. That's what we need to finish legal incorporation enough to get a real Board of Directors set up. So there's a meaning to the number.
    * Lack of projects: Do we want to try and get a sign-up form ready before we try to sign up more projects? Will it make us look unprofessional if we don't have that ready when we're asking them to sign-up?
    * Honestly, we can just make it a text document or email to fill out. That will work. It doesn't need to be a schmancy dynamic form.
    * Hoping to get full email integration and update notifications ready soon on the technical end.
    * Time-line for having money coming in? 
        * Early 2015 to have at least some money coming in is David's hope, based on how long he can go without another day job.
        * Aaron thinks more realistic hope to have the site fully operational by LibrePlanet.
    * Launching fund-drive on Black Friday / Buy Nothing Day could be spun in a good way.
    * So aim for taking it live the Tuesday before Thanksgiving (avoid stressful workday on Thanksgiving itself).
    * Endorsements and testimonials would be nice but should not delay the start of the campaign. We can add those as we go.
    * Compelling video and decently developed website showing progress is more important than any particular type of promotion.
     * People don't have to comprehensively understand our process, just think we're interesting and worth keeping an eye on and chipping in a few bucks.
    * Multiple videos is also a good way to hold interest in a campaign, by posting them throughout the fundraising period. A longer video could be a good addition to the homepage anyway, so we can redirect people there if a short video catches their attention.
    * Pre-launch publicity? Don't sweat it.
    * Once we hit $3000, we can get Deb Olsen working and continue to raise funds.
