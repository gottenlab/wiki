---
title: Best Practices for Community Engagement
categories: community
...

With the right steps, projects can maximize their connection to the general public, sustain their user base, and convert users into long-term patrons and creative contributors.

This page focuses on how to engage with patrons and end users. You can find advice for engaging with volunteers and project team members at the [team-engagement](team-engagement) page.

## Provide summaries of everything

Use headings over paragraphs, quick summaries of longer texts, etc. Keep things skimmable and concise.

*Summary of this page: To maintain patron engagement, keep everyone informed of progress and provide summaries wherever possible.*

## Update regularly

Keep team members, patrons, and Snowdrift.coop staff regularly updated on your progress and pitfalls! 

You don't want unpleasant surprises among those supposedly working towards the same goal. Team members feel more accountable and valued when you communicate about what you're doing, why, and how they fit into the picture. If everyone goes into their separate corners to work, you may find yourself needlessly duplicating others' work or some members may incorrectly feel that they are pulling more than their own weight. Regular communication helps generate new ideas and potential solutions to current problems.

Patrons who know what a project does with their funds will more likely continue or even increase their support. It's disappointing when a project doesn't meet a stated schedule of goals, but clear communication along with effort to resolve the problem will encourage patrons to feel that their support still matters.

Keep Snowdrift.coop in the loop as well! We are here to help you achieve your goals; we can refer you to all sorts of helpful resources; and if you have complaints about us, we want to know so that we can address the issue as best we can. We are a work in progress, too!
