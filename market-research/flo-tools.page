---
title: Free/Libre/Open Tools
...

*Note: This list is currently a work in progress.*

The following software projects are ones we use or have used ourselves and so are upstream projects that enable our work.

## Communication

* [CiviCRM](https://civicrm.org/) (AGPL) — constituent relationship management application to organize volunteer and donor information
* [Discourse](http://discourse.org/) (GPLv2) — forum software for community discussion and planning
* [IRC at freenode.net](https://freenode.net/) — Internet Relay Chat network that supports FLO projects and communities
* [Jitsi Meet](https://jitsi.org/Projects/JitsiMeet) (Apache License 2.0) — video conferencing with webRTC for meetings
* [Mumble](https://wiki.mumble.info/wiki/Main_Page) (BSD) — low-latency voice chat application


## Design

* [GIMP](http://www.gimp.org/) (GPL) — bitmap graphics editor, used for photo
  editing and general image manipulation
* [Inkscape](https://inkscape.org/) (GPL) — vector graphics editor, used to
  create the images for the site, including website mockups and illustrations
* [Kdenlive](https://kdenlive.org/) (GPLv2+) — non-linear video editor, used to
  create introductory videos on the site
* [Krita](http://krita.org/) (GPLv2+) — sketching and painting application; also used to convert image color space for printing
* [Pencil](http://pencil.evolus.vn/) (GPLv2) — used to make wireframes and prototypes
* [pngquant](https://pngquant.org/) (BSD) — utility for lossy compression of PNG files for web
* [Scribus](http://scribus.net/) (GPL) — layout and desktop publishing software with support for vector and bitmap formats; used to convert image colorspace  for printing
* [Simple Scan](https://launchpad.net/simple-scan) (GPLv3) — scanning utility a with batch scan, page reordering and other features, used to scan in paper  sketches for inking

We've used the following applications for animation:

* [Blender](https://www.blender.org/) (GPLv2+) — 3D production suite with a  modeler, compositor, rendering engine as well as full non-linear video editor and animation support for both 3D and some 2D
* [Natron](http://natron.fr/) (GPLv2) — an extensible compositor with support for a number of file formats
* [Synfig](http://www.synfig.org) (GPL) — 2D animation software with HDRI and sound support

And the following for music production (a few select items of many):

* [Ardour](https://www.ardour.org/) full Digital Audio Workstation (DAW)
* [KXStudio](https://kxstudio.linuxaudio.org/) a full music-focused repository set for GNU/Linux
* [Hydrogen drum machine](http://hydrogen-music.org/)
* [Calf audio plugins](https://calf-studio-gear.org/)

## Development

* [GitLab CE](https://about.gitlab.com/community/) (MIT) — repository hosting and code collaboration. (Unfortunately *not* a fully FLO project as they have a restricted proprietary Enterprise version)

## Documentation and project management

* [Etherpad](http://etherpad.org/) (Apache License 2.0) — collaborative drafting of project specifications and other documents
* [Gitit](https://github.com/jgm/gitit) (GPLv2) — wiki used to store information on various aspects of the project, including planning and research

## Server infrastructure

* [Apache](http://www.apache.org/) (Apache License 2.0) — web server
* [Docker](https://www.docker.com/) (Apache License 2.0) — enables software containerization and templates for faster deployment
* [Nginx](http://nginx.org/) (BSD) — web server and reverse proxy
