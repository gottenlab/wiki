---
title: Snowdrift.coop Intro for Educators
toc: false
categories: communications, intro
...

## Your focus

You understand the transformative power of education, and you want everyone to have access to the best resources for learning You've probably got a huge collection of books, posters, and other teaching aids for your classroom that you paid for yourself or even that you made yourself. Your top priority is to share your knowledge with the world and continue learning yourself.

## Your problems

Developing curricula and the teaching resources to support them is intensive work and you get little or no support for your time spent on these things outside the classroom. You want up-to-date materials but publishers of textbooks, educational software, and other proprietary resources charge everyone all over again after every small change, making the cost prohibitive. Publishers also place complex restrictions on copying and sharing materials, thus placing legal barriers to combining the best ideas from different sources. You wish you could have more control over the matierials you use in the classroom and more freely collaborate and share with others.

Today, you rely more and more on technology. Not only do students use various devices and programs in their studies, but you handle records, scheduling, and other administrative tasks with complex software. You have little control over this technology. Licensing and DRM restrictions get in the way when you need to work on different computers or exchange files between different systems. You feel locked-in to certain tools now that you have all your records in proprietary formats, but you don't know how else to operate.

## How we can help you

Like you, Snowdrift.coop supports quality education for all. As part of the Open Educational Resources movement, we believe that development costs should not determine access to education, even (perhaps especially) for those in non-traditional learning environments. Materials should be freely translatable and otherwise open to adaptation for particular needs. All educational projects on Snowdrift.coop are not only free to access but also liberated from excessive copyright restrictions, so you can share and compile what you find works best with your colleagues without fear of legal action.

The Snowdrift.coop funding mechanism provides a way to cover the costs of developing materials and even to potentially make a living as an author of educational resources without adding proprietary restrictions. Our long-term pledging model supports continuous updates and improvements so that resources remain relevant. In fact, operating outside of the traditional publishing system allows for new information or standards to be incorporated more quickly. End-users may even make and submit their own updates.

As you surely know, teaching others can be the best way to learn. With FLO resources, students can improve the materials and add their own contributions. Our collaboration tools can help students coordinate with others around the world to build the best teaching materials for everyone.

The software projects we support may include a wide range of tools useful in your teaching — all free from the hassle of proprietary restrictions.

## How you can help us

We need projects! We want to include educational projects in our initial beta testing. If you are interested yourself or know someone who might be, please sign up for the site and send us a message!

Spread the word about Snowdrift.coop and Open Educational Resources (and FLO software), and make use of them in classroom whenever you can! The best way to grow the movement is to prove its viability on the ground.

Finally, we would like to include schools, school districts, and Universities as *institutional members*. Imagine a school district using our matching pledge: "We want to switch to fully FLO software and textbooks throughout our classrooms but the existing resources are not adequate. We can fund some development but have a very tight budget. So, via Snowdrift.coop, we pledge to add more funds to these FLO projects to match *other school districts* who also commit. If we all switch to FLO together, our combined resources can provide amazing results!" Let us know if you have experience with the governance and budgeting parts of these institutions and want to help us figure out how to make this vision a reality.
